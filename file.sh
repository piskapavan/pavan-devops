pipeline {
    agent any

    stages {
        stage("Checkout code") {
            steps {
                checkout(...)
            }
        }
        stage("Build and push Docker image") {
            steps {
                sh "aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 793510399628.dkr.ecr.us-east-1.amazonaws.com"
                sh "docker build -t python ."
                sh "docker tag python 793510399628.dkr.ecr.us-east-1.amazonaws.com/us-east-1:jenkins"
                sh "docker push 793510399628.dkr.ecr.us-east-1.amazonaws.com/python:jenkins"
            }
        }
    }
}
